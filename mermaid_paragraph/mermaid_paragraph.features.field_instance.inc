<?php
/**
 * @file
 * mermaid_paragraph.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function mermaid_paragraph_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'paragraphs_item-paragraph_mermaid-field_chart'.
  $field_instances['paragraphs_item-paragraph_mermaid-field_chart'] = array(
    'bundle' => 'paragraph_mermaid',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'mermaid_field',
        'settings' => array(),
        'type' => 'default',
        'weight' => 0,
      ),
      'paragraphs_editor_preview' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'paragraphs_item',
    'field_name' => 'field_chart',
    'label' => 'Chart',
    'required' => FALSE,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'module' => 'mermaid_field',
      'settings' => array(),
      'type' => 'mermaid_field_textarea',
      'weight' => 2,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Chart');

  return $field_instances;
}
