<?php
/**
 * @file
 * mermaid_paragraph.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function mermaid_paragraph_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_chart'.
  $field_bases['field_chart'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_chart',
    'indexes' => array(),
    'locked' => 0,
    'module' => 'mermaid_field',
    'settings' => array(),
    'translatable' => 0,
    'type' => 'mermaid_field',
  );

  return $field_bases;
}
