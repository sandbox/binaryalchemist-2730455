<?php
/**
 * @file
 * mermaid_paragraph.features.inc
 */

/**
 * Implements hook_paragraphs_info().
 */
function mermaid_paragraph_paragraphs_info() {
  $items = array(
    'paragraph_mermaid' => array(
      'name' => 'paragraph_mermaid',
      'bundle' => 'paragraph_mermaid',
      'locked' => '1',
    ),
  );
  return $items;
}
